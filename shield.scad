use <obiscad/bcube.scad>

module sbox(ext_size=[50,50,50], tickness=2, ext_bevel=4, cres=24){
    int_size=[
        ext_size[0]-tickness*2,
        ext_size[1]-tickness*2,
        ext_size[2]-tickness
    ];
    
    int_bevel=ext_bevel-tickness;
    
    ovl_tickness=tickness/2;
    ovl_bevel=ext_bevel-ovl_tickness;
    ovl_size=[
        ext_size[0]-ovl_tickness*2,
        ext_size[1]-ovl_tickness*2,
        tickness
    ];
    
	  difference(){
		    bcube(ext_size, ext_bevel, cres);
		    translate([0, 0, tickness]){
			      bcube(int_size, int_bevel, cres);
		    }
        translate([0, 0, ext_size[2]/2]){
			      bcube(ovl_size, ovl_bevel, cres);
		    }
	  }
}

module spad(ext_dia=5, int_dia=2.5, height=10, out_len=5, out_num=4, out_tickness, out_height=5, cres=32){
    ext_r=ext_dia/2;
    int_r=int_dia/2;
    tickness=ext_r-int_r;
    mid_r=(ext_r+int_r)/2;
    
    out_tickness=out_tickness?out_tickness:tickness*2/out_num;
    
    echo(out_height);
    
    union(){
        difference(){
            cylinder(height, r=ext_r, $fn=cres);
            cylinder(height+tickness, r=int_r, $fn=cres);
        }
        for(i = [0: out_num]){
            rotate(360/out_num*i, [0, 0, 1]){
                translate([mid_r, 0, 0]){
                    polyhedron(points=[
                            [0, out_tickness, 0],
                            [0, out_tickness, out_height],
                            [out_len, out_tickness, 0],
                            [0, -out_tickness, 0],
                            [0, -out_tickness, out_height],
                            [out_len, -out_tickness, 0]
                        ], triangles=[
                            [0, 2, 1],
                            [3, 4, 5],
                            [0, 1, 3], [3, 1, 4],
                            [1, 2, 4], [4, 2, 5],
                            [3, 5, 0], [5, 2, 0],
                        ]);
                }
            }
        }
    }
}

size=[50,50,10];
union(){
    translate([0, 0, size[2]/2]){
        sbox(size,1);
    }
    intersection(){
        translate([0, 0, 6]){
            bcube(size, 4, 12);
        }
        union(){
            for(point=[
                    [20, 20, 1],
                    [-20, 20, 1],
                    [-20, -20, 1],
                    [20, -20, 1],
                ]){
                translate(point){
                    spad(height=size[2]-1, out_num=8);
                }
            }
        }
    }
}
